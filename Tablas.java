/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tablas;

import java.util.Scanner;

/**
 *
 * @author Administrador
 */
public class Tablas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese el numero para realizar la tabla");
        int dato = scan.nextInt();
        sumar(dato);
        restar(dato);
        multiplicar(dato);
        dividir(dato);
    }

    public static void sumar(int dato) {
        System.out.println("Tabla de sumar " + dato);
        for (int i = 1; i <= 12; i++) {
            System.out.println(dato + " + " + i + " = " + (dato + i));
        }
    }

    public static void restar(int dato) {
        System.out.println("Tabla de restar" + dato);
        for (int i = 1; i <= 12; i++) {
            System.out.println(dato + " - " + i + " = " + (dato - i));
        }

    }

    public static void multiplicar(int dato) {
        System.out.println("Tabla de multiplicar " + dato);
        for (int i = 1; i <= 12; i++) {
            System.out.println(dato + " x " + i + " = " + (dato * i));
        }

    }

    public static void dividir(int dato) {
        System.out.println("Tabla de dividir " + dato);
        for (int i = 1; i <= 12; i++) {
            System.out.println(dato + " / " + i + " = " + (dato / i));
        }
    }
}